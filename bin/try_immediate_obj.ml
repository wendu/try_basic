let instant_obj =
  object
    val mutable x = 1
    method increase = x <- x + 1
    method decrease = x <- x - 1
    method result = x
  end

let go () =
  instant_obj#increase;
  instant_obj#increase;
  print_int instant_obj#result;
  print_endline ""
