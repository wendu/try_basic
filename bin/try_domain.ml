let square n = n * n
let x = 5
let y = 10

let _ =
  let count = Domain.recommended_domain_count () in
  Printf.printf "recommand domain count: %d\n" count;
  let worker1 = Domain.spawn (fun () -> square 5) in
  let result = Domain.join worker1 in
  Printf.printf "worker returns %d\n" result
