module Wrong = struct
  type value = Int of int | Bool of bool

  type 'a expr =
    | Value of value
    | Equal of int expr * int expr
    | Add of int expr * int expr
    | If of bool expr * 'a expr * 'a expr

  exception IllType

  let rec eval (type t) (expr : t expr) =
    match expr with
    | Value v -> v
    | Equal (x, y) -> (
        match (eval x, eval y) with
        | Bool _, _ | _, Bool _ -> raise IllType
        | Int f1, Int f2 -> Bool (f1 = f2))
    | If (c, t, e) -> (
        match eval c with
        | Bool b -> if b then eval t else eval e
        | Int _ -> raise IllType)
    | Add (x, y) -> (
        match (eval x, eval y) with
        | Bool _, _ | _, Bool _ -> raise IllType
        | Int f1, Int f2 -> Int (f1 + f2))

  (* Above not type safe, lots of runtime check, easy to create ill typed expression *)
end

type _ value = Int : int -> int value | Bool : bool -> bool value

type _ expr =
  | Value : 'a value -> 'a expr
  | Eq : int expr * int expr -> bool expr
  | Plus : int expr * int expr -> int expr
  | If : bool expr * 'a expr * 'a expr -> 'a expr

let expr3 = Plus (Value (Int 5), Value (Int 5))

let eval_value (type a) (v : a value) : a =
  match v with Int x -> x | Bool b -> b

(* let eval expr (type a) (x : a expr) =
   match x with
   | Value v -> eval_value v
   | If (c, t, e) -> if eval c then eval t else eval e
   | Eq (x, y) -> eval x = eval y
   | Plus (x, y) -> eval x + eval y *)
