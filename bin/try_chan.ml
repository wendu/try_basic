open Domainslib

let c = Domainslib.Chan.make_unbounded ()

let read_channel c id =
  let message = Chan.recv c in
  print_endline (message ^ string_of_int id)

let write_channel c =
  let message = "I am " in
  Chan.send c message

let go () =
  let domains =
    List.init 5 (fun id -> Domain.spawn (fun () -> read_channel c id))
  in
  write_channel c;
  write_channel c;
  write_channel c;
  write_channel c;
  write_channel c;
  List.iter Domain.join domains
